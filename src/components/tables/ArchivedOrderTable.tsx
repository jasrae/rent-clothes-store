import * as React from 'react';
import {alpha} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import {visuallyHidden} from '@mui/utils';
import {grey} from "@mui/material/colors";
import {Button} from "@mui/material";
import {NumericFormat} from "react-number-format";

interface Data {
    id: string;
    name: string;
    phone: string;
    status: string;
    quantity: number;
    totalPrice: number;
    dateCreated: string;
    dateDelivered: string;
    remaining: number;
    note: string;
}

function createData(
    id: string,
    name: string,
    phone: string,
    status: string,
    quantity: number,
    totalPrice: number,
    dateCreated: string,
    dateDelivered: string,
    remaining: number,
    note: string
): Data {
    return {
        id,
        name,
        phone,
        status,
        quantity,
        totalPrice,
        dateCreated,
        dateDelivered,
        remaining,
        note
    };
}

const rows = [
    createData('O013', "Nguyễn Thị Minh Khai", "0937756897", "Hoàn Thành", 10, 1000000, "19/07/23", "19/07/23", 0, "Khi giao, các em giao đến tận địa chỉ này giúp chị: 12 Nguyễn Thị Minh Khai, phường 21, quận 1, TP. Hồ Chí Minh."),
    createData('O014', "Trường Sơn", "0937567888", "Hoàn Thành", 10, 1000000, "18/07/23", "18/07/23", 200000, "Vui lòng gọi trước nửa tiếng."),
    createData('O015', "Quang Trung", "0937522345", "Hoàn Thành", 15, 1500000, "18/07/23", "18/07/23", 500000, "Lên lai quần giúp anh theo các số đo như sau (cm): 12, 10, 13, 5, 7. Còn lại thì cứ để như size hiện tại của em"),
    createData('O016', "Nguyễn Văn Trỗi", "0937557811", "Hoàn Thành", 5, 1500000, "15/07/23", "15/07/23", 0, "Gọi cho em 0937557811 lúc đồ sẵn sàng ạ."),
    createData('O017', "Nguyễn Tất Thành", "0919286653", "Hoàn Thành", 2, 400000, "15/07/23", "15/07/23", 0, "Lên lai quần giúp em (cm) 20, 15"),
    createData('O018', "Cách Mạng Tháng Chín", "0919322478", "Hoàn Thành", 5, 1500000, "14/07/23", "14/07/23", 0, "Em sẽ gọi sau để xác định ngày giao ạ."),
    createData('O019', "Hoàng Văn Thụ", "0919874328", "Hoàn Thành", 3, 150000, "14/07/23", "15/07/23", 0, "Em sẽ gọi sau để xác định ngày giao ạ."),
    createData('O020', "Hồng Hà", "0919209224", "Hoàn Thành", 2, 400000, "14/07/23", "16/07/23", 0, ""),
]

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
): (
    a: { [key in Key]: number | string },
    b: { [key in Key]: number | string },
) => number {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) {
            return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
    disablePadding: boolean;
    id: keyof Data;
    label: string;
    numeric: boolean;
}

const headCells: readonly HeadCell[] = [
    {
        id: 'id',
        numeric: false,
        disablePadding: false,
        label: 'Mã',
    },
    {
        id: 'name',
        numeric: false,
        disablePadding: true,
        label: 'Tên khách hàng',
    },
    {
        id: 'phone',
        numeric: false,
        disablePadding: false,
        label: 'Số diện thoại',
    },
    {
        id: 'status',
        numeric: false,
        disablePadding: false,
        label: 'Trạng thái',
    },
    {
        id: 'quantity',
        numeric: true,
        disablePadding: false,
        label: 'Số lượng',
    },
    {
        id: 'totalPrice',
        numeric: true,
        disablePadding: false,
        label: 'Thành tiền',
    },
    {
        id: 'dateCreated',
        numeric: false,
        disablePadding: false,
        label: 'Ngày tạo',
    },
    {
        id: 'dateDelivered',
        numeric: false,
        disablePadding: false,
        label: 'Ngày giao',
    },
    {
        id: 'note',
        numeric: false,
        disablePadding: false,
        label: 'Ghi chú',
    },
];

interface EnhancedTableProps {
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
    order: Order;
    orderBy: string;
}

function EnhancedTableHead(props: EnhancedTableProps) {
    const {order, orderBy, onRequestSort } =
        props;
    const createSortHandler =
        (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
            onRequestSort(event, property);
        };

    return (
        <TableHead>
            <TableRow color={grey[900]} className="bg-slate-300">
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align="center"
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            <label className="font-bold text-lg">{headCell.label}</label>
                            {orderBy === headCell.id ? (
                                <Box component="span" sx={visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Box>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

interface EnhancedTableToolbarProps {
    numSelected: number;
}

function EnhancedTableToolbar(props: EnhancedTableToolbarProps) {
    const { numSelected } = props;

    return (
        <Toolbar
            className="bg-slate-300"
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 },
                ...(numSelected > 0 && {
                    bgcolor: (theme) =>
                        alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
                }),
            }}
        >
            <Typography
                sx={{ flex: '1 1 100%' }}
                variant="h6"
                id="tableTitle"
                component="div"
            >
                <label className="font-bold text-3xl">Hoá đơn</label>
            </Typography>
        </Toolbar>
    );
}

export default function ArchivedOrderTable() {
    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof Data>('id');
    const [selected, setSelected] = React.useState<readonly string[]>([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof Data,
    ) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected: readonly string[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const isSelected = (name: string) => selected.indexOf(name) !== -1;

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const visibleRows = React.useMemo(
        () =>
            stableSort(rows, getComparator(order, orderBy)).slice(
                page * rowsPerPage,
                page * rowsPerPage + rowsPerPage,
            ),
        [order, orderBy, page, rowsPerPage],
    );

    return (
        <Box sx={{ width: '100%' }}>
            <Paper sx={{ width: '100%', mb: 2 }}>
                <EnhancedTableToolbar numSelected={selected.length} />
                <TableContainer>
                    <Table
                        sx={{ minWidth: 750 }}
                        aria-labelledby="tableTitle"
                    >
                        <EnhancedTableHead
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                        />
                        <TableBody>
                            {visibleRows.map((row, index) => {
                                const isItemSelected = isSelected(row.name);
                                const labelId = `enhanced-table-checkbox-${index}`;

                                return (
                                    <TableRow
                                        hover
                                        onClick={(event) => handleClick(event, row.name)}
                                        role="checkbox"
                                        aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={row.name}
                                        selected={isItemSelected}
                                        sx={{ cursor: 'pointer' }}
                                    >
                                        <TableCell
                                            component="th"
                                            id={labelId}
                                            scope="row"
                                            padding="normal"
                                        >
                                            {row.id}
                                        </TableCell>
                                        <TableCell align="left">{row.name}</TableCell>
                                        <TableCell align="left">{row.phone}</TableCell>
                                        <TableCell align="left">
                                            <Button variant="contained" color={row.status === "Chưa cọc" ? "error" :
                                                row.status === 'Chưa giao' ? 'info' :
                                                    row.status === 'Đã giao' ? 'primary' :
                                                        row.status === 'Nhận đồ' ? 'success' : 'error'}
                                                className="w-full"
                                            >
                                                <label className="font-bold w-full">{row.status}</label>
                                            </Button>
                                        </TableCell>
                                        <TableCell align="center">{row.quantity}</TableCell>
                                        <TableCell>
                                            <NumericFormat
                                                disabled
                                                value={row.totalPrice}
                                                suffix={' VND'}
                                                type="text"
                                                thousandSeparator
                                                className="bg-white"
                                            />
                                        </TableCell>
                                        <TableCell>{row.dateCreated}</TableCell>
                                        <TableCell>{row.dateDelivered}</TableCell>
                                        <TableCell><p className="w-20 line-clamp-1">{row.note}</p></TableCell>
                                    </TableRow>
                                );
                            })}
                            {emptyRows > 0 && (
                                <TableRow
                                    style={{
                                        height: 53 * emptyRows,
                                    }}
                                >
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 15, 20, 25]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>
        </Box>
    );
}
