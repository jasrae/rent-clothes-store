import * as React from 'react';
import {alpha} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import DeleteIcon from '@mui/icons-material/Delete';
import {grey} from "@mui/material/colors";
import {TextField} from "@mui/material";
import {NumericFormat} from "react-number-format";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../store";
import {OrderProductModel} from "../models/OrderProductModel";

interface HeadCell {
    id: keyof OrderProductModel;
    label: string;
    numeric: boolean;
}

const headCells: readonly HeadCell[] = [
    {
        id: 'id',
        numeric: false,
        label: 'Mã',
    },
    {
        id: 'name',
        numeric: false,
        label: 'Tên',
    },
    {
        id: 'quantity',
        numeric: true,
        label: 'Số lượng',
    },
    {
        id: 'price',
        numeric: true,
        label: 'Đơn giá',
    },
    {
        id: 'totalPrice',
        numeric: true,
        label: 'Tổng tiền',
    },
    {
        id: 'isSelling',
        numeric: false,
        label: 'Bán',
    },
];

interface EnhancedTableProps {
    numSelected: number;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
    rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
    const { onSelectAllClick, numSelected, rowCount } = props;
    return (
        <TableHead>
            <TableRow color={grey[900]} className="bg-slate-300">
                <TableCell padding="checkbox">
                    <Checkbox
                        color="primary"
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{
                            'aria-label': 'select all desserts',
                        }}
                    />
                </TableCell>
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align='center'
                    >
                        <label className="font-bold text-lg truncate">{headCell.label}</label>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

interface EnhancedTableToolbarProps {
    numSelected: number;

    onDelete: () => void;
}

function EnhancedTableToolbar(props: EnhancedTableToolbarProps) {
    const { numSelected, onDelete} = props;

    return (
        <Toolbar
            className="bg-slate-300"
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 },
                ...(numSelected > 0 && {
                    bgcolor: (theme) =>
                        alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
                }),
            }}
        >
            {numSelected > 0 ? (
                <Typography
                    sx={{ flex: '1 1 100%' }}
                    color="inherit"
                    variant="subtitle1"
                    component="div"
                >
                    {numSelected} selected
                </Typography>
            ) : (
                <Typography
                    sx={{ flex: '1 1 100%' }}
                    variant="h6"
                    id="tableTitle"
                    component="div"
                >
                    <label className="font-bold text-3xl">Thông tin sản phẩm</label>
                </Typography>
            )}
            {numSelected > 0 ? (
                <Tooltip title="Delete">
                    <IconButton onClick={onDelete}>
                        <DeleteIcon/>
                    </IconButton>
                </Tooltip>
            ): <div/>}
        </Toolbar>
    );
}

// @ts-ignore
function OrderProductTable() {
    const [selected, setSelected] = React.useState<readonly string[]>([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [forSale, setForSale] = React.useState<readonly string[]>([]);
    const products = useSelector((state : AppState) => state.listProduct.data);
    const dispatch = useDispatch();

    const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelected = products.map((n) => n.id);
            setSelected(newSelected);
            return;
        }
        setSelected([]);
    };

    const handleClick = (id: string) => {
        const idx = selected.indexOf(id);
        let rowSelected : readonly string[] = [];
        if (idx === -1) {
            rowSelected = rowSelected.concat(selected, id);
        } else if (idx === 0) {
            rowSelected = rowSelected.concat(selected.slice(1));
        } else if (idx === selected.length - 1) {
            rowSelected = rowSelected.concat(selected.slice(0, -1));
        } else if (idx > 0) {
            rowSelected = rowSelected.concat(
                selected.slice(0, idx),
                selected.slice(idx + 1)
            );
        }

        setSelected(rowSelected);
    };

    const handleCheckForSale = (id: string) => {
        let idx = forSale.indexOf(id);
        let saleProducts : readonly string[] = [];
        if (idx === -1) {
            saleProducts = saleProducts.concat(forSale, id);
        } else if (idx === 0) {
            saleProducts = saleProducts.concat(forSale.slice(1));
        } else if (idx === forSale.length - 1) {
            saleProducts = saleProducts.concat(forSale.slice(0, -1));
        } else if (idx > 0) {
            saleProducts = saleProducts.concat(
                forSale.slice(0, idx),
                forSale.slice(idx + 1)
            );
        }

        setForSale(saleProducts);
    }

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const isSelected = (id: string) => selected.indexOf(id) !== -1;
    const isForSale = (id: string) => forSale.indexOf(id) !== -1;

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - products.length) : 0;

    const handleQuantityChange = (index: number) => (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        const newItems = [...products];
        newItems[index].quantity = Number(event.target.value);
        newItems[index].totalPrice = newItems[index].quantity * newItems[index].price;
        dispatch({type: "update_list_product", payload: newItems});
    };

    const handlePriceChange = (index: number, changedValue: string) => {
        const newItems = [...products];
        newItems[index].price = Number(changedValue);
        newItems[index].totalPrice = newItems[index].quantity * newItems[index].price;
        dispatch({type: "update_list_product", payload: newItems});
    };

    const onDelete = () => {
        console.log("Item ready for delete: ", selected);
        let newItems = products.filter((item) => !isSelected(item.id));
        setSelected([]);
        dispatch({type: "update_list_product", payload: newItems});
    }

    return (
        <Box sx={{ width: '100%' }}>
            <Paper sx={{ width: '100%', mb: 2 }}>
                <EnhancedTableToolbar numSelected={selected.length} onDelete={onDelete} />
                <TableContainer>
                    <Table
                        sx={{ minWidth: 750 }}
                        aria-labelledby="tableTitle"
                    >
                        <EnhancedTableHead
                            numSelected={selected.length}
                            onSelectAllClick={handleSelectAllClick}
                            rowCount={products.length}
                        />
                        <TableBody>
                            {products.map((row, index) => {
                                const isItemSelected = isSelected(row.id);
                                const labelId = `enhanced-table-checkbox-${index}`;

                                return (
                                    <TableRow
                                        hover
                                        tabIndex={-1}
                                        key={row.id}
                                        sx={{ cursor: 'pointer' }}
                                    >
                                        <TableCell padding="checkbox">
                                            <Checkbox
                                                aria-label="tesst"
                                                color="primary"
                                                checked={isItemSelected}
                                                key={row.id}
                                                onChange={() => handleClick(row.id)}
                                                inputProps={{
                                                    'aria-labelledby': labelId,
                                                }}
                                            />
                                        </TableCell>
                                        <TableCell
                                            component="th"
                                            id={labelId}
                                            scope="row"
                                            padding="none"
                                        >
                                            {row.id}
                                        </TableCell>
                                        <TableCell align="left">{row.name}</TableCell>
                                        <TableCell align="left">
                                            <TextField type="number" value={row.quantity}
                                                       onChange={handleQuantityChange(index)}
                                                       className="w-20"
                                            />
                                        </TableCell>
                                        <TableCell align="left">
                                            <NumericFormat
                                                disabled={!isForSale(row.id)}
                                                value={row.price}
                                                customInput={TextField}
                                                suffix={' VND'}
                                                type="text"
                                                thousandSeparator
                                                onValueChange={values => {
                                                    handlePriceChange(index, values.value);
                                                }}
                                            />
                                        </TableCell>
                                        <TableCell align="left">
                                            <NumericFormat
                                                disabled
                                                value={row.totalPrice}
                                                customInput={TextField}
                                                suffix={' VND'}
                                                type="text"
                                                thousandSeparator
                                            />
                                        </TableCell>
                                        <TableCell id="test" align="left" padding="checkbox">
                                            <Checkbox
                                                id="test"
                                                color="primary"
                                                onChange={() => handleCheckForSale(row.id)}
                                            />
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                            {emptyRows > 0 && (
                                <TableRow
                                    style={{
                                        height: 53 * emptyRows,
                                    }}
                                >
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={products.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>
        </Box>
    );
}

export default OrderProductTable;
