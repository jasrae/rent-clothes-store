import * as React from 'react';
import {alpha} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import {visuallyHidden} from '@mui/utils';
import {grey} from "@mui/material/colors";
import {NumericFormat} from "react-number-format";

interface Data {
    id: string;
    reportDate: string;
    nonCash: number;
    cash: number;
    expense: number;
    total: number;
}

function createData(
    id: string,
    reportDate: string,
    nonCash: number,
    cash: number,
    expense: number,
    total: number
): Data {
    return {
        id,
        reportDate,
        nonCash,
        cash,
        expense,
        total
    };
}

const rows = [
    createData('01', "15/07/23", 42000000, 10000000, 534000, 534000),
    createData('02', "14/07/23", 23000000, 7000000, 277000, 277000),
    createData('03', "13/07/23", 27000000, 1000000, 253000, 253000),
    createData('04', "12/07/23", 33000000, 15000000, 200000, 200000),
    createData('05', "11/07/23", 11000000, 13000000, 200000, 200000),
    createData('06', "10/07/23", 90000000, 3000000, 333000, 333000),
    createData('07', "09/07/23", 100000000, 25000000, 450000, 450000),
    createData('08', "08/07/23", 10000000, 50000000, 450000, 450000),
    createData('09', "07/07/23", 10000000, 55000000, 230000, 230000),
    createData('10', "06/07/23", 100000000, 55000000, 230000, 230000),
    createData('11', "05/07/23", 100000000, 55000000, 230000, 230000),
    createData('12', "04/07/23", 330000000, 5000000, 230000, 230000),
    createData('13', "03/07/23", 300000000, 5000000, 200000, 200000),
    createData('14', "02/07/23", 200000000, 6000000, 200000, 200000),
    createData('15', "01/07/23", 20000000, 7000000, 200000, 200000),
]

interface HeadCell {
    disablePadding: boolean;
    id: keyof Data;
    label: string;
    numeric: boolean;
}

const headCells: readonly HeadCell[] = [
    {
        id: 'reportDate',
        numeric: true,
        disablePadding: true,
        label: 'Ngày',
    },
    {
        id: 'nonCash',
        numeric: false,
        disablePadding: false,
        label: 'Chuyển khoản',
    },
    {
        id: 'cash',
        numeric: false,
        disablePadding: false,
        label: 'Tiền mặt',
    },
    {
        id: 'expense',
        numeric: false,
        disablePadding: false,
        label: 'Phát sinh',
    },
    {
        id: 'total',
        numeric: false,
        disablePadding: false,
        label: 'Tổng doanh thu',
    },
];

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
): (
    a: { [key in Key]: number | string },
    b: { [key in Key]: number | string },
) => number {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) {
            return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

interface EnhancedTableProps {
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
    order: Order;
    orderBy: string;
}

function EnhancedTableHead(props: EnhancedTableProps) {
    const {order, orderBy, onRequestSort} =
        props;
    const createSortHandler =
        (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
            onRequestSort(event, property);
        };

    return (
        <TableHead>
            <TableRow color={grey[900]} className="bg-slate-300">
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align="center"
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            <label className="font-bold text-lg">{headCell.label}</label>
                            {orderBy === headCell.id ? (
                                <Box component="span" sx={visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Box>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

interface EnhancedTableToolbarProps {
    numSelected: number;
}

function EnhancedTableToolbar(props: EnhancedTableToolbarProps) {
    const {numSelected} = props;

    return (
        <Toolbar
            className="bg-slate-300"
            sx={{
                pl: {sm: 2},
                pr: {xs: 1, sm: 1},
                ...(numSelected > 0 && {
                    bgcolor: (theme) =>
                        alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
                }),
            }}
        >
            <Typography
                sx={{flex: '1 1 100%'}}
                variant="h6"
                id="tableTitle"
                component="div"
            >
                <label className="font-bold text-3xl">Doanh thu</label>
            </Typography>
        </Toolbar>
    );
}

export default function ReportTable() {
    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof Data>('id');
    const [selected, setSelected] = React.useState<readonly string[]>([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof Data,
    ) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected: readonly string[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const isSelected = (name: string) => selected.indexOf(name) !== -1;

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const visibleRows = React.useMemo(
        () =>
            stableSort(rows, getComparator(order, orderBy)).slice(
                page * rowsPerPage,
                page * rowsPerPage + rowsPerPage,
            ),
        [order, orderBy, page, rowsPerPage],
    );

    return (
        <Box sx={{width: '100%'}}>
            <Paper sx={{width: '100%', mb: 2}}>
                <EnhancedTableToolbar numSelected={selected.length}/>

                <TableContainer>
                    <Table
                        sx={{minWidth: 750}}
                        aria-labelledby="tableTitle"
                    >
                        <EnhancedTableHead
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                        />
                        <TableBody>
                            {visibleRows.map((row) => {
                                const isItemSelected = isSelected(row.reportDate);
                                return (
                                    <TableRow
                                        hover
                                        onClick={(event) => handleClick(event, row.id)}
                                        role="checkbox"
                                        aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={row.id}
                                        selected={isItemSelected}
                                        sx={{cursor: 'pointer'}}
                                    >
                                        <TableCell align="center">{row.reportDate}</TableCell>
                                        <TableCell align="center">
                                            <NumericFormat
                                                disabled
                                                value={row.nonCash}
                                                suffix={' VND'}
                                                type="text"
                                                thousandSeparator
                                                className="bg-white"
                                            />
                                        </TableCell>
                                        <TableCell align="center">
                                            <NumericFormat
                                                disabled
                                                value={row.cash}
                                                suffix={' VND'}
                                                type="text"
                                                thousandSeparator
                                                className="bg-white"
                                            />
                                        </TableCell>
                                        <TableCell align="center">
                                            <NumericFormat
                                                disabled
                                                value={row.expense}
                                                suffix={' VND'}
                                                type="text"
                                                thousandSeparator
                                                className="bg-white"
                                            />
                                        </TableCell>
                                        <TableCell align="center">
                                            <NumericFormat
                                                disabled
                                                value={row.total}
                                                suffix={' VND'}
                                                type="text"
                                                thousandSeparator
                                                className="bg-white"
                                            />
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                            {emptyRows > 0 && (
                                <TableRow
                                    style={{
                                        height: 53 * emptyRows,
                                    }}
                                >
                                    <TableCell colSpan={6}/>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 15, 20, 25]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>
        </Box>
    );
}
