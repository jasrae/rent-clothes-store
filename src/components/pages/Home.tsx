import Sidebar from "../Sidebar";
import SummaryCard from "../SummaryCard";
import React from "react";

function Home() {

    return (
        <div className="App">
            <div className="bg-white">
                <div className="flex">
                    <Sidebar selected="home"/>
                    <div className="container ml-60 mx-5 mt-6">
                        <div className="grid gap-6 lg:grid-cols-3">
                            <SummaryCard label="Total Users" value="30"/>
                            <SummaryCard label="Total Orders" value="150"/>
                            <SummaryCard label="Total Products" value="200"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home;
