import Sidebar from "../Sidebar";
import {Button} from "@mui/material";
import {PlusCircleIcon} from "@heroicons/react/24/outline";
import React, {useState} from "react";
import ExpenseTable from "../tables/ExpenseTable";
import ExpenseForm from "../forms/ExpenseForm";

function Expense() {
    const [creatingExpense, setCreatingExpense] = useState("hidden");

    function openExpenseForm() {
        setCreatingExpense("")
    }

    function closeExpenseForm() {
        setCreatingExpense("hidden");
    }

    return (
        <div className="App">
            <div className="bg-white">
                <div className="flex">
                    <Sidebar selected="expense"/>
                    <div className="container ml-60 mx-5 mt-6">
                        <div className="grid gap-6 lg:grid-cols-4">
                            {/*Header*/}
                            <div className="lg:col-span-3 ml-8"/>
                            <div className="lg:col-span-1 flex">
                                <Button variant="contained"
                                        onClick={openExpenseForm}
                                        className="rounded-lg border border-3">
                                    <PlusCircleIcon className=" w-10 ml-4 font-bold" />
                                    <span className="w-full rounded-lg font-bold text-lg mr-2">Thêm phát sinh</span>
                                </Button>
                            </div>
                        </div>

                        {/*Create new Expense*/}
                        <ExpenseForm open={creatingExpense} onClose={closeExpenseForm}/>

                        {/*Expense List*/}
                        <div className="mx-8 my-10">
                            <ExpenseTable/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Expense;
