import Sidebar from "../Sidebar";
import React from "react";
import ReportTable from "../tables/ReportTable";

function Report() {

    return (
        <div className="App">
            <div className="bg-white">
                <div className="flex">
                    <Sidebar selected="report"/>
                    <div className="container ml-60 mx-5 mt-6">
                        <div className="mx-8 my-10">
                            <ReportTable/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Report;
