import Sidebar from "../Sidebar";
import React, {useState} from "react";
import {Autocomplete, MenuItem, Select, TextField} from "@mui/material";
import ArchivedOrderTable from "../tables/ArchivedOrderTable";

const searchOptions = [
    {
        "label" : "Mã hoá đơn",
        "key" : "id"
    },
    {
        "label" : "Tên khách hàng",
        "key" : "customerName"
    },
    {
        "label" : "Ngày giao",
        "key" : "deliverDate"
    }
]

const orderIds = [
    "O0013",
    "O0014",
    "O0015",
    "O0016",
    "O0017",
    "O0018",
    "O0019",
    "O0020",
    "O0021",
    "O0022",
]

function Archive() {
    const [search, setSearch] = useState(searchOptions[0].key);

    function changeSearchOption(option : string){
        setSearch(option);
    }

    return (
        <div className="App">
            <div className="bg-white">
                <div className="flex">
                    <Sidebar selected="archive"/>
                    <div className="container ml-60 mx-5 mt-6">
                        <div className="grid gap-6 lg:grid-cols-4">
                            <div className="lg:col-span-2 ml-8 ">
                                <div className="grid lg:grid-cols-3 hidden">
                                    <Select className="lg:col-span-1 items-center font-bold rounded-lg shadow bg-slate-300"
                                            value={search} onChange={e => changeSearchOption(e.target.value)}>
                                        {searchOptions.map((item) => {
                                            return <MenuItem key={item.key} className="text-lg" value={item.key}>
                                                {item.label}
                                            </MenuItem>
                                        })}
                                    </Select>
                                    <Autocomplete
                                        className="lg:col-span-2 "
                                        renderInput={(params) =>
                                            <TextField {...params} label="Search ..." className="w-full pl-4 py-6" />}
                                        options={orderIds}/>
                                </div>
                            </div>
                            <div className="lg:col-span-1"/>
                        </div>

                        <div className="flex bg-slate-200 bg-transparent mx-4 my-8">
                            <div className="w-full h-full rounded-lg">
                                <ArchivedOrderTable/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Archive;
