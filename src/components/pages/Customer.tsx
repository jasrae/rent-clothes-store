import Sidebar from "../Sidebar";
import React, {useState} from "react";
import {PlusCircleIcon} from "@heroicons/react/24/outline";
import {Autocomplete, Button, MenuItem, Select, TextField} from "@mui/material";
import CustomerTable from "../tables/CustomerTable";
import CustomerForm from "../forms/CustomerForm";

const searchOptions = [
    {
        label : "Tên",
        id : "name"
    },
    {
        label : "Số điện thoại",
        id : "phone"
    }
]

const customerNames = [
    "Nguyễn Thị Minh Khai",
    "Nguyễn Thái Sơn",
    "Phạm Ngũ Lão"
]

function Customer() {
    const [search, setSearch] = useState(searchOptions[0].id);
    const [creatingCustomer, setCreatingCustomer] = useState("hidden");

    function changeSearchOption(option: string) {
        setSearch(option);
    }

    function openCustomerForm() {
        setCreatingCustomer("")
    }

    function closeCustomerForm() {
        setCreatingCustomer("hidden")
    }

    return (
        <div className="App">
            <div className="bg-white">
                <div className="flex">
                    <Sidebar selected="customer"/>
                    <div className="container ml-60 mx-5 mt-6">
                        <div className="grid gap-6 lg:grid-cols-4">
                            {/*Header*/}
                            <div className="lg:col-span-2 ml-8 ">
                                <div className="grid lg:grid-cols-3 hidden">
                                    <Select
                                        className="lg:col-span-1 items-center font-bold rounded-lg shadow bg-slate-300"
                                        value={search} onChange={e => changeSearchOption(e.target.value)}>
                                        {searchOptions.map((item) => {
                                            return <MenuItem className="text-lg" value={item.id} key={item.id}>{item.label}</MenuItem>
                                        })}
                                    </Select>
                                    <Autocomplete
                                        className="lg:col-span-2 "
                                        renderInput={(params) =>
                                            <TextField {...params} label="Search ..." className="w-full pl-4 py-6" />}
                                        options={customerNames}/>
                                </div>
                            </div>
                            <div className="lg:col-span-1"/>
                            <div className="lg:col-span-1 flex">
                                <Button variant="contained"
                                        onClick={openCustomerForm}
                                        className="rounded-lg border border-3">
                                    <PlusCircleIcon className=" w-10 ml-4 font-bold" />
                                    <span className="w-full rounded-lg font-bold text-lg mr-2">Thêm khách hàng</span>
                                </Button>
                            </div>
                        </div>

                        {/*Customer Form*/}
                        <CustomerForm open={creatingCustomer} onClose={closeCustomerForm}/>

                        {/*Customer List*/}
                        <div className="mx-8 my-10">
                            <CustomerTable/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Customer;
