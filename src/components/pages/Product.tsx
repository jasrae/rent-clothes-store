import Sidebar from "../Sidebar";
import {Autocomplete, Button, MenuItem, Select, TextField} from "@mui/material";
import {PlusCircleIcon} from "@heroicons/react/24/outline";
import React, {useState} from "react";
import ProductTable from "../tables/ProductTable";
import ProductForm from "../forms/ProductForm";

const searchOptions = [
    {
        label : "Mã sản phẩm",
        id : "id"
    },
    {
        label : "Tên",
        id : "name"
    }
]

const productNames = [
    "Áo dài đỏ",
    "Áo dài xuân",
    "Sườn xám",
    "Sườn xám cách tân",
    "Áo dài xẻ cách tân",
    "Áo dài xẻ cách tân phối màu",
    "Áo dài trung niên",
    "Áo dài nam trọn bộ",
    "Áo dài phong cách học sinh",
    "Áo dài phong cách học sinh Trung Quốc",
    "Áo dài phong cách học sinh Nhật Bản",
    "Áo dài phong cách học sinh cách tân",
]

function Product() {
    const [search, setSearch] = useState(searchOptions[0].id);
    const [creatingProduct, setCreatingProduct] = useState("hidden");

    function changeSearchOption(value : string) {
        setSearch(value);
    }

    function openProductForm() {
        setCreatingProduct("");
    }

    function closeProductForm() {
        setCreatingProduct("hidden");
    }

    return (
        <div className="App">
            <div className="bg-white">
                <div className="flex">
                    <Sidebar selected="product"/>
                    <div className="container ml-60 mx-5 mt-6">
                        <div className="grid lg:grid-cols-4">
                            {/*Header*/}
                            <div className="lg:col-span-2 ml-8 ">
                                <div className="grid lg:grid-cols-3 hidden">
                                    <Select
                                        className="lg:col-span-1 items-center font-bold rounded-lg shadow bg-slate-300"
                                        value={search} onChange={e => changeSearchOption(e.target.value)}>
                                        {searchOptions.map((item) => {
                                            return <MenuItem className="text-lg" value={item.id} key={item.id}>
                                                {item.label}
                                            </MenuItem>
                                        })}
                                    </Select>
                                    <Autocomplete
                                        className="lg:col-span-2 "
                                        renderInput={(params) =>
                                            <TextField {...params} label="Search ..." className="w-full pl-4 py-6" />}
                                        options={productNames}/>
                                </div>
                            </div>
                            <div className="lg:col-span-1"/>
                            <div className="lg:col-span-1">
                                <Button variant="contained"
                                        onClick={openProductForm}
                                        className="rounded-lg border border-3">
                                    <PlusCircleIcon className=" w-10 ml-4 font-bold" />
                                    <span className="w-full rounded-lg font-bold text-lg mr-2">Thêm sản phẩm</span>
                                </Button>
                            </div>
                        </div>

                        <ProductForm open={creatingProduct} onClose={closeProductForm} />

                        <div className="mx-8 my-10">
                            <ProductTable/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Product;
