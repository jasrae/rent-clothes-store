import Sidebar from "../Sidebar";
import React, {useState} from "react";
import {PlusCircleIcon} from "@heroicons/react/24/outline";
import {Autocomplete, Button, MenuItem, Select, TextField} from "@mui/material";
import OrderTable from "../tables/OrderTable";
import OrderForm from "../forms/OrderForm";

const searchOptions = [
    {
        "label" : "Mã hoá đơn",
        "key" : "id"
    },
    {
        "label" : "Tên khách hàng",
        "key" : "customerName"
    },
    {
        "label" : "Ngày giao",
        "key" : "deliverDate"
    }
]

const orderIds = [
    "O0001",
    "O0002",
    "O0003",
    "O0004",
    "O0005",
    "O0006",
    "O0007",
    "O0008",
    "O0009",
    "O0010",
    "O0011",
    "O0012",
]

function Order() {
    const [search, setSearch] = useState(searchOptions[0].key);
    const [creatingOrder, setCreatingOrder] = useState("hidden");

    function changeSearchOption(option : string){
        setSearch(option);
    }

    function openCreateOrderForm() {
        setCreatingOrder("");
    }

    function closeCreateOrderForm() {
        setCreatingOrder("hidden");
    }

    return (
        <div className="App">
            <div className="bg-white">
                <div className="flex">
                    <Sidebar selected="order"/>
                    <div className="container ml-60 mx-5 mt-6">
                        {/*Header - Search bar and button create order*/}
                        <div className="grid gap-6 lg:grid-cols-4">
                            <div className="lg:col-span-2 ml-8 ">
                                <div className="grid lg:grid-cols-3 hidden">
                                    <Select className="lg:col-span-1 items-center font-bold rounded-lg shadow bg-slate-300"
                                            value={search} onChange={e => changeSearchOption(e.target.value)}>
                                        {searchOptions.map((item) => {
                                            return <MenuItem key={item.key} className="text-lg" value={item.key}>{item.label}</MenuItem>
                                        })}
                                    </Select>
                                    <Autocomplete
                                        className="lg:col-span-2 "
                                        renderInput={(params) =>
                                            <TextField {...params} label="Search ..." className="w-full pl-4 py-6" />}
                                        options={orderIds}/>
                                </div>
                            </div>
                            <div className="lg:col-span-1"/>
                            <div className="lg:col-span-1 flex">
                                <Button variant="contained"
                                        onClick={openCreateOrderForm}
                                        className="rounded-lg border border-3">
                                    <PlusCircleIcon className=" w-10 ml-4 font-bold" />
                                    <span className="w-full rounded-lg font-bold text-lg mr-2">Thêm hoá đơn</span>
                                </Button>
                            </div>
                        </div>

                        {/*Create new Order*/}
                        <OrderForm open={creatingOrder} onClose={closeCreateOrderForm}/>

                        {/*Order List*/}
                        <div className="flex bg-slate-200 bg-transparent mx-4 my-8">
                            <div className="w-full h-full rounded-lg">
                                <OrderTable/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Order;
