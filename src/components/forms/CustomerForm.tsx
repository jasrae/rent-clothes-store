import {Button, TextField} from "@mui/material";
import {XCircleIcon} from "@heroicons/react/24/outline";
import React, {useState} from "react";
import {CustomerModel} from "../models/CustomerModel";

// @ts-ignore
function CustomerForm({open, onClose}) {
    let customer = new CustomerModel();
    const [name, setName] = useState(customer.name);
    const [phone, setPhone] = useState(customer.phone);
    const [address, setAddress] = useState(customer.address);

    const clearData = () => {
        setName(customer.name);
        setPhone(customer.phone);
        setAddress(customer.address);
    }

    const submit = () => {
        const params = new CustomerModel({id: "", name: name, phone: phone, address: address});
        console.log("Submit params: ", params);
        clearData();
        onClose();
    }

    return (
        <div className={open}>
            <div className="grid gap-6 mt-8 lg:grid-cols-8">
                <div className="lg:col-span-1"/>
                <div className="lg:col-span-6">
                    <div className="grid lg:grid-cols-6 rounded-lg shadow shadow-2xl drop-shadow-md shadow-gray-800 bg-slate-300">
                        <div className="lg:col-span-2 ml-2 my-2">
                            <label className="font-bold text-gray-600 text-4xl flex">Khách hàng</label>
                        </div>
                        <div className="lg:col-span-4 place-self-end mr-2">
                            <Button color="error" variant="contained" onClick={() => onClose()}>
                                <XCircleIcon/>
                            </Button>
                        </div>
                        <div className="lg:col-span-2 ml-2 my-2">
                            <TextField label="Họ và tên"
                                       value={name || ''}
                                       onChange={e => setName(e.target.value)}
                                       className="w-full h-full bg-white rounded-lg font-semibold"/>
                        </div>
                        <div className="lg:col-span-1 ml-2 my-2">
                            <TextField label="Số điện thoại"
                                       value={phone || ''}
                                       onChange={e => setPhone(e.target.value)}
                                       className="w-full pl-4 py-6 bg-white rounded-lg font-semibold"/>
                        </div>
                        <div className="lg:col-span-3 ml-2 my-2 mr-2">
                            <TextField label="Địa chỉ"
                                       value={address || ''}
                                       onChange={e => setAddress(e.target.value)}
                                       className="w-full pl-4 py-6 bg-white rounded-lg font-semibold"/>
                        </div>
                        <div className="lg:col-span-4 "/>
                        <div className="lg:col-span-2 my-2 mr-2">
                            <div className="grid lg:grid-cols-2 gap-4">
                                <Button variant="contained" onClick={submit}>Lưu</Button>
                                <Button variant="contained" color="error" onClick={() => onClose()}>Huỷ</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CustomerForm;
