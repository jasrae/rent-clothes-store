import {Button, Select, TextField} from "@mui/material";
import {XCircleIcon} from "@heroicons/react/24/outline";
import React, {useState} from "react";
import {ProductModel} from "../models/ProductModel";

const statuses = [
    {
        id: "AVAILABLE",
        label: "Tồn kho",
        color: "bg-green-600"
    },
    {
        id: "RENTED",
        label: "Cho thuê",
        color: "bg-cyan-500"
    }
]

// @ts-ignore
function ProductForm({open, onClose}) {
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [quantity, setQuantity] = useState(0);
    const [price, setPrice] = useState(0);
    const [status, setStatus] = useState(statuses[0].id);

    const clearAll = () => {
        setId('');
        setName('');
        setQuantity(0);
        setPrice(0);
        setStatus(statuses[0].id);
    }

    const submit = () => {
        let product = new ProductModel({id: id, name: name, quantity: quantity, price: price, status: status});
        console.log("Submit params: ", product);
        clearAll();
        onClose();
    }

    return (
        <div className={open}>
            <div className="grid lg:grid-cols-6 mt-6">
                <div className="lg:col-span-1"/>
                <div className="lg:col-span-4">
                    <div className="grid lg:grid-cols-6 bg-slate-300 rounded-lg shadow shadow-2xl drop-shadow-md shadow-gray-800">
                        <div className="lg:col-span-2 ml-2 my-2">
                            <label className="font-bold text-gray-600 text-4xl flex">Sản phẩm</label>
                        </div>
                        <div className="lg:col-span-4 place-self-end mr-2">
                            <Button color="error" variant="contained" onClick={() => onClose()}>
                                <XCircleIcon/>
                            </Button>
                        </div>
                        <div className="lg:col-span-1 ml-2 my-2">
                            <TextField label="Mã" className="bg-white rounded-lg" value={id || ''}
                                       onChange={e => setId(e.target.value)}/>
                        </div>
                        <div className="lg:col-span-2 ml-2 my-2">
                            <TextField label="Tên" className="bg-white rounded-lg w-full" value={name || ''}
                                       onChange={e => setName(e.target.value)}/>
                        </div>
                        <div className="lg:col-span-1 ml-2 my-2">
                            <TextField label="Số lượng" className="bg-white rounded-lg" type="number"
                                       value={quantity || 0} onChange={e => setQuantity(parseInt(e.target.value))}/>
                        </div>
                        <div className="lg:col-span-1 ml-2 my-2">
                            <TextField label="Đơn giá" className="bg-white rounded-lg" type="number"
                                       value={price || 0} onChange={e => setPrice(parseInt(e.target.value))}/>
                        </div>
                        <div className="lg:col-span-1 ml-2 my-2 mr-2">
                            <Select
                                className={"lg:col-span-1 items-center font-bold rounded-lg shadow w-full bg-white "}
                                value={status} onChange={e => setStatus(e.target.value)} >
                                {statuses.map((item) => {
                                    return (
                                        <Button variant="text"
                                                value={item.id} key={item.id} className=" w-full place-self-center">
                                            <label className="font-bold">{item.label}</label>
                                        </Button>
                                )})}
                            </Select>
                        </div>
                        <div className="lg:col-span-6 place-self-end mb-4 mr-2">
                            <div className="grid lg:grid-cols-2 gap-2">
                                <div className="lg:col-span-1 rounded-lg">
                                    <Button variant="contained" onClick={submit}>
                                        <label className="font-bold text-l">Lưu</label>
                                    </Button>
                                </div>
                                <div className="lg:col-span-1 rounded-lg">
                                    <Button color="error" variant="contained" onClick={() => onClose()}>
                                        <label className="font-bold text-l">Huỷ</label>
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="lg:col-span-1"/>
            </div>
        </div>
    )
}

export default ProductForm;
