import {Button, Checkbox, FormControlLabel, InputAdornment, MenuItem, Select, TextField} from "@mui/material";
import {PlusCircleIcon, XCircleIcon} from "@heroicons/react/24/outline";
import SearchIcon from "@mui/icons-material/Search";
import OrderProductTable from "../tables/OrderProductTable";
import React, {useState} from "react";
import ProductModals from "../modals/ProductModals";
import {Provider, useDispatch, useSelector} from "react-redux";
import store, {AppState} from "../store";
import {NumericFormat} from "react-number-format";
import moment from "moment";
import {OrderProductModel} from "../models/OrderProductModel";


const buyOptions = [
    {
        "label" : "Mua tại cửa hàng",
        "id" : "atStore"
    },
    {
        "label" : "Mua qua mạng",
        "id" : "online"
    }
]

const depositOptions = [
    {
        "label" : "Giấy tờ tuỳ thân",
        "id" : "IDs"
    },
    {
        "label" : "Tiền",
        "id" : "money"
    }
]


// @ts-ignore
function OrderForm({open, onClose}) {
    const [buyOption, setBuyOption] = useState(buyOptions[0].id);
    const [depositOption, setDepositOption] = useState(depositOptions[0].id);
    const [orderId, setOrderId] = useState('');
    const [customerName, setCustomerName] = useState('');
    const [customerPhone, setCustomerPhone] = useState('');
    const [customerAddress, setCustomerAddress] = useState('');
    const [paidFull, setPaidFull] = useState(false);
    const [paidForward, setPaidForward] = useState(0);
    const [paidForwardNonCash, setPaidForwardNonCash] = useState(false);
    const [remaining, setRemaining] = useState(0);
    const [deliveryDateDefined, setDeliveryDateDefined] = useState(true);
    const [deliveryDate, setDeliveryDate] = useState(new Date());
    const [returnedDate, setReturnedDate] = useState(new Date());
    const [depositByCash, setDepositByCash] = useState(false);
    const [depositMoney, setDepositMoney] = useState(0);
    const [notes, setNotes] = useState('');
    const [openedProductModal, setOpenedProductModal] = useState(false);
    const products = useSelector((state: AppState) => state.listProduct.data);
    const totalPrice = useSelector((state: AppState) : number =>
        Object.values(state.listProduct.data).reduce((sum, item) => sum + item.totalPrice, 0));
    const dispatch = useDispatch();

    const openProductModal = () => {
        setOpenedProductModal(true);
    }

    const closeProductModal = () => {
        setOpenedProductModal(false);
        console.log("Total Price: ", totalPrice);
    }

    const onChangeDefinedDeliveryDate = () => {
        setDeliveryDateDefined(!deliveryDateDefined);
    }

    const onSubmit = () => {
        let data = {
            orderId : orderId,
            buyOption: buyOption,
            customerName : customerName,
            customerPhone : customerPhone,
            customerAddress: customerAddress,
            products: products,
            totalPrice: totalPrice,
            paidForward: paidForward,
            paidForwardNonCash: paidForwardNonCash,
            remaining: remaining,
            deliveryDateDefined: deliveryDateDefined,
            deliveryDate: deliveryDate,
            returnedDate : returnedDate,
            depositOption: depositOption,
            depositMoney: depositMoney,
            depositByCash: depositByCash,
            notes: notes
        }
        console.log("Submit params: ", data);
        clearData();
        onClose();
    }

    const clearData = () => {
        setOrderId('');
        setBuyOption(buyOptions[0].id);
        setCustomerName('');
        setCustomerPhone('');
        setCustomerAddress('');
        dispatch({type: 'update_list_product', payload: []});
        dispatch({type: 'update_product', payload: new OrderProductModel()});
        setPaidForward(0);
        setPaidFull(false);
        setRemaining(0);
        setDeliveryDate(new Date());
        setDeliveryDateDefined(true);
        setReturnedDate(new Date());
        setDepositOption(depositOptions[0].id);
        setDepositMoney(0);
        setDepositByCash(false);
        setNotes('');
    }

    return (
        <div className={open}>
            <Provider store={store}>
                <ProductModals open={openedProductModal} onClose={closeProductModal}/>
            </Provider>
            <div className="grid gap-6 mt-8 lg:grid-cols-8">
                <div className="lg:col-span-1"/>
                <div className="lg:col-span-6">
                    <div className="grid lg:grid-cols-6 rounded-lg shadow shadow-2xl drop-shadow-md shadow-gray-800 bg-slate-300">
                        <div className="lg:col-span-2 ml-2 my-2">
                            <label className="font-bold text-4xl flex">Hoá đơn</label>
                        </div>
                        <div className="lg:col-span-4 place-self-end mr-2">
                            <Button color="error" variant="contained" onClick={() => onClose()}>
                                <XCircleIcon/>
                            </Button>
                        </div>
                        <div className="lg:col-span-1 ml-2 my-2">
                            <TextField label="Mã hoá đơn" className="bg-white rounded-lg" value={orderId}
                                       onChange={event => setOrderId(event.target.value)}/>
                        </div>
                        <div className="lg:col-span-5 place-self-end mr-2 ml-2 my-2">
                            <Select className="lg:col-span-1 items-center font-bold rounded-lg shadow bg-white"
                                    value={buyOption} onChange={e => setBuyOption(e.target.value)}>
                                {buyOptions.map((item) => {
                                    return <MenuItem key={item.id} className="text-lg" value={item.id}>{item.label}</MenuItem>
                                })}
                            </Select>
                        </div>
                        <div className="lg:col-span-2 ml-2 my-2">
                            <div className="grid lg:grid-cols-4 ">
                                <TextField label="Tên" className="lg:col-span-3 bg-white" value={customerName}
                                           onChange={e => setCustomerName(e.target.value)}/>
                                <Button className="lg:col-span-1" color="primary" variant="contained">
                                    <SearchIcon/>
                                </Button>
                            </div>
                        </div>
                        <div className="lg:col-span-1 ml-2 my-2">
                            <TextField label="Số điện thoại" className="bg-white" value={customerPhone}
                                       onChange={e => setCustomerPhone(e.target.value)}/>
                        </div>
                        <div className="lg:col-span-3 ml-2 my-2 mr-2">
                            <TextField label="Địa chỉ" className="bg-white w-full" value={customerAddress}
                                       onChange={e => setCustomerAddress(e.target.value)}/>
                        </div>
                        <div className="lg:col-span-2 ml-2 my-2">
                            <label className="font-bold text-gray-600 text-3xl flex">Sản phẩm</label>
                        </div>
                        <div className="lg:col-span-4 place-self-end mr-2 ml-2 my-2">
                            <Button color="primary" variant="contained" onClick={openProductModal}>
                                <PlusCircleIcon className=" w-10 ml-4 font-bold" />
                                <label className="text-lg font-white">Thêm sản phẩm</label>
                            </Button>
                        </div>
                        <div className="lg:col-span-6 mr-2 ml-2 my-2">
                            <Provider store={store}>
                                <OrderProductTable/>
                            </Provider>
                        </div>
                        <div className="lg:col-span-6 ml-2 my-2">
                            <div className="grid lg:grid-cols-8 ">
                                <div className="lg:col-span-3 my-2">
                                    <NumericFormat
                                        label="Thành tiền"
                                        disabled
                                        value={totalPrice || 0}
                                        customInput={TextField}
                                        suffix={' VND'}
                                        type="text"
                                        thousandSeparator
                                        className="bg-white w-full"
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <FormControlLabel
                                                        value="end"
                                                        control={
                                                            <Checkbox
                                                                id="paidFull"
                                                                color="primary"
                                                                className="lg:col-span-1"
                                                                value={paidFull}
                                                                onChange={() => {
                                                                    if (!paidFull) {
                                                                        setPaidFull(true);
                                                                        setPaidForward(totalPrice);
                                                                    } else {
                                                                        setPaidFull(false);
                                                                    }

                                                                }}
                                                            />
                                                        }
                                                        label={<label className="font-bold">Trả hết</label>}
                                                        labelPlacement="end"/>
                                                </InputAdornment>

                                            )
                                        }}
                                    />
                                </div>
                                <div className="lg:col-span-3 ml-2 my-2">
                                    <NumericFormat
                                        label="Trả trước"
                                        disabled={paidFull}
                                        value={paidForward}
                                        customInput={TextField}
                                        suffix={' VND'}
                                        type="text"
                                        thousandSeparator
                                        className="bg-white w-full"
                                        onValueChange={values => {
                                            setPaidForward(parseInt(values.value));
                                        }}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <FormControlLabel
                                                        value="end"
                                                        control={
                                                            <Checkbox
                                                                id="paidForwardNonCash"
                                                                color="primary"
                                                                className="lg:col-span-1"
                                                                value={paidForwardNonCash}
                                                                onChange={() => setPaidForwardNonCash(true)}
                                                            />
                                                        }
                                                        label={<label className="font-bold">Chuyển khoản</label>}
                                                        labelPlacement="end"/>
                                                </InputAdornment>

                                            )
                                        }}
                                    />
                                </div>
                                <div className="lg:col-span-2 ml-2 mr-2 my-2">
                                    <NumericFormat
                                        disabled
                                        label="Còn lại"
                                        value={totalPrice !== undefined ? totalPrice - paidForward : 0}
                                        customInput={TextField}
                                        suffix={' VND'}
                                        type="text"
                                        thousandSeparator
                                        className="bg-white w-full"
                                        onChange={event => setRemaining(parseInt(event.target.value))}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="lg:col-span-2 ml-2 my-2">
                            <TextField disabled={!deliveryDateDefined} type="date" label="Ngày giao"
                                       className="bg-white w-full"
                                       InputLabelProps={{ shrink: true }}
                                       value={moment(deliveryDate).format("YYYY-MM-DD")}
                                       onChange={event => setDeliveryDate(new Date(event.target.value))}
                                       InputProps={{
                                           startAdornment: (
                                               <InputAdornment position="start">
                                                   <FormControlLabel
                                                       value="end"
                                                       control={<Checkbox value={!deliveryDateDefined} onChange={onChangeDefinedDeliveryDate}/>}
                                                       label={<label className="font-bold">Chưa xác định</label>}
                                                       labelPlacement="end"/>
                                               </InputAdornment>

                                           )
                                       }}
                            />
                        </div>
                        <div className="lg:col-span-1 ml-2 my-2">
                            <TextField type="date" label="Ngày trả"
                                       value={moment(deliveryDate).add(3, 'd').format("YYYY-MM-DD")}
                                       onChange={event => setReturnedDate(new Date(event.target.value))}
                                       className="bg-white w-full"
                                       InputLabelProps={{ shrink: true }}/>
                        </div>
                        <div className="lg:col-span-1 ml-2 my-2">
                            <Select className="lg:col-span-1 items-center font-bold rounded-lg shadow bg-white w-full text-xl text-gray-900"
                                    value={depositOption} onChange={e => setDepositOption(e.target.value)} aria-label="Cọc" label="Cọc">
                                {depositOptions.map((item) => {
                                    return <MenuItem aria-label="Cọc" key={item.id} className="text-lg" value={item.id}>{item.label}</MenuItem>
                                })}
                            </Select>
                        </div>
                        <div className="lg:col-span-2 ml-2 my-2 mr-2">
                            <NumericFormat
                                disabled={depositOption === "IDs"}
                                label="Tiền cọc"
                                value={depositMoney}
                                customInput={TextField}
                                suffix={' VND'}
                                type="text"
                                thousandSeparator
                                className="bg-white"
                                onChange={event => setDepositMoney(parseInt(event.target.value))}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <FormControlLabel
                                                value="end"
                                                control={
                                                    <Checkbox value={depositByCash} disabled={depositOption === "IDs"}
                                                              onChange={() => setDepositByCash(true)}/>
                                                }
                                                label={<label className="font-bold">Chuyển khoản</label>}
                                                labelPlacement="end"/>
                                        </InputAdornment>

                                    )
                                }}
                            />
                        </div>
                        <div className="lg:col-span-6 ml-2 my-2 mr-2">
                            <TextField multiline label="Ghi chú" className="bg-white w-full" minRows="2" maxRows="3"
                                       value={notes} onChange={event => setNotes(event.target.value)}/>
                        </div>
                        <div className="lg:col-span-6 my-2 mr-2 place-self-end">
                            <div className="grid lg:grid-cols-2 gap-4">
                                <Button variant="contained" onClick={() => onSubmit()}>Lưu</Button>
                                <Button variant="contained" color="error" onClick={() => onClose()}>Huỷ</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default OrderForm;
