import {Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import React, {useState} from "react";
import {OrderProductModel} from "../models/OrderProductModel";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../store";
import {NumericFormat} from "react-number-format";

// @ts-ignore
function ProductModals({open, onClose}) {
    const [name, setName] = useState('');
    const [id, setId] = useState('');
    const [quantity, setQuantity] = useState(0);
    const [price, setPrice] = useState(0);
    const [store, setStore] = useState(30);
    const dispatch = useDispatch();
    const products = useSelector((state: AppState) => state.listProduct.data);

    const clearData = () => {
        setName('');
        setId('');
        setQuantity(0);
        setPrice(0);
        setStore(30);
    }

    const submit = () => {
        console.log("Current products: ", products);
        let orderProduct = new OrderProductModel(
            {id : id, name: name, quantity: quantity, price: price, totalPrice: 0, isSelling: false});
        products.push(orderProduct);
        console.log("Order Product: ", orderProduct);
        dispatch({type: 'update_list_product', payload: products});
        clearData();
        onClose();
    }

    const handleChangeQuantity = (value: string) => {
        let qty = parseInt(value);
        setQuantity(qty);
        setStore(qty === 0 ? (store + quantity) :
            quantity === qty ? store :
                quantity > qty ? (store + (quantity - qty)) : (store - (qty - quantity)));
    }

    return (
        <Dialog open={open} onClose={onClose} maxWidth="md">
            <div className="rounded-full">
                <DialogTitle className="bg-slate-300 text-lg">
                    <label className="text-3xl font-bold ">Tìm sản phẩm</label>
                </DialogTitle>
                <DialogContent className=" grid mt-4 lg:grid-cols-12">
                    <div className="lg:col-span-5 my-2">
                        <div className="grid lg:grid-cols-5 ">
                            <TextField label="Tên" className="lg:col-span-4 bg-white" value={name}
                                       onChange={e => setName(e.target.value)}/>
                            <Button className="lg:col-span-1" color="primary" variant="contained">
                                <SearchIcon/>
                            </Button>
                        </div>
                    </div>
                    <div className="lg:col-span-3 ml-2 my-2 mr-2">
                        <div className="grid lg:grid-cols-4 ">
                            <TextField label="Mã" className="lg:col-span-3 bg-white" value={id}
                                       onChange={e => setId(e.target.value)}/>
                            <Button className="lg:col-span-1" color="primary" variant="contained">
                                <SearchIcon/>
                            </Button>
                        </div>
                    </div>
                    <div className="lg:col-span-2 ml-4 my-2">
                        <TextField type="number" label="Số lượng" value={quantity}
                                   onChange={e => handleChangeQuantity(e.target.value)}/>
                    </div>
                    <div className="lg:col-span-2 ml-2 my-2">
                        <NumericFormat
                            label="Đơn giá"
                            value={price}
                            customInput={TextField}
                            suffix={' VND'}
                            type="text"
                            thousandSeparator
                            onValueChange={values => {
                                setPrice(parseInt(values.value));
                            }}
                        />
                    </div>
                </DialogContent>
                <DialogActions>
                    <div className="grid lg:grid-cols-3 gap-4 place-content-stretch">
                        <div className="lg:col-span-1 my-2 align-middle place-items-center">
                            <label className="text-red-500 font-bold text-xl w-full ">Tồn kho: {store}</label>
                        </div>
                        <div className="lg:col-span-2 gap-2">
                            <div className="grid lg:grid-cols-2 gap-2">
                                <Button variant="contained" onClick={submit}>Lưu</Button>
                                <Button variant="contained" color="error" onClick={onClose}>Huỷ</Button>
                            </div>
                        </div>
                    </div>
                </DialogActions>
            </div>
        </Dialog>
    );
}

export default ProductModals;
