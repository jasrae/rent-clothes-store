import {
    ArchiveBoxArrowDownIcon,
    ArrowLeftOnRectangleIcon,
    BanknotesIcon,
    CurrencyDollarIcon,
    DocumentTextIcon,
    HomeIcon,
    ShoppingBagIcon,
    UserIcon
} from "@heroicons/react/24/outline";

const items = [
    {
        id : "home",
        label : "Trang chủ",
        path : "/",
        selected : false,
        icon : <HomeIcon className="w-12"/>
    },
    {
        id : "order",
        label : "Hoá đơn",
        path : "/order",
        selected : false,
        icon : <DocumentTextIcon className="w-12"/>
    },
    {
        id : "customer",
        label : "Khách hàng",
        path : "/customer",
        selected : false,
        icon : <UserIcon className="w-12"/>
    },
    {
        id : "product",
        label : "Sản phẩm",
        path : "/product",
        selected : false,
        icon : <ShoppingBagIcon className="w-12"/>
    },
    {
        id : "archive",
        label : "Lưu trữ",
        path : "/archive",
        selected : false,
        icon : <ArchiveBoxArrowDownIcon className="w-12"/>
    },
    {
        id : "expense",
        label : "Phát sinh",
        path : "/expense",
        selected : false,
        icon : <CurrencyDollarIcon className="w-12"/>
    },
    {
        id : "report",
        label : "Doanh thu",
        path : "/report",
        selected : false,
        icon : <BanknotesIcon className="w-12"/>
    },
    {
        id : "logout",
        label : "Đăng xuất",
        path : "/logout",
        selected : false,
        icon : <ArrowLeftOnRectangleIcon className="w-12"/>
    },
]

function Sidebar(props: {selected: string}) {
    const isMobile = () => /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    const MENU_MOBILE = "flex hover:bg-red-600 w-18";
    const MENU_WEB = "flex hover:bg-red-600 w-56";
    const MENU = isMobile() ? MENU_MOBILE : MENU_WEB;
    const SELECTED_MENU_MOBILE = "flex bg-red-600 w-18";
    const SELECTED_MENU_WEB = "flex bg-red-600 w-56";
    const SELECTED_MENU = isMobile() ? SELECTED_MENU_MOBILE : SELECTED_MENU_WEB;
    const FIRST_DIV_WEB = "fixed flex h-screen navbar bg-slate-300 rounded-r-3xl place-items-start w-60";
    const FIRST_DIV_MOBILE = "fixed flex h-screen navbar bg-slate-300 rounded-r-3xl place-items-start w-20";
    const UL_WEB = "menu rounded-box items-start w-56";
    const UL_MOBILE = "menu rounded-box items-start w-18";
    items.forEach(item => {
        if (item.id === props.selected) item.selected = true
    });

    return (
        <div className={isMobile() ? FIRST_DIV_MOBILE : FIRST_DIV_WEB}>
            <ul className={isMobile() ? UL_MOBILE : UL_WEB}>
                <li className="my-8"><label className="font-extrabold text-3xl">Nica 1993</label></li>
                {items.map((item) => {
                    return <li key={item.id} className="ml-4 mb-4 place-self-center">
                        <a key={item.id} href={item.path} className={item.selected ? SELECTED_MENU : MENU}>
                            {item.icon}
                            {isMobile() ? <div/> : <label className="font-bold text-lg place-self-center ml-4">{item.label}</label>}
                        </a>
                    </li>
                })}
            </ul>
        </div>
    );
}

export default Sidebar;
