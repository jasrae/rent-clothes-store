export class OrderProductModel {
    id: string = ''
    name: string = ''
    quantity: number = 0
    price: number = 0
    totalPrice: number = 0
    isSelling: boolean = false

    constructor(params: OrderProductModel = {} as OrderProductModel) {
        const {
            id = '',
            name = '',
            quantity = 0,
            price = 0,
            isSelling = false
        } = params;

        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.totalPrice = this.quantity * this.price;
        this.isSelling = isSelling;
    }
}
