export class ExpenseModel {
    id: string = ''
    content: string = ''
    isSpent: boolean = false

    constructor(params: ExpenseModel = {} as ExpenseModel) {
        const {
            id = '',
            content = '',
            isSpent = false
        } = params;

        this.id = id;
        this.content = content;
        this.isSpent = isSpent;
    }
}
