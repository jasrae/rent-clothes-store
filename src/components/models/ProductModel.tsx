export class ProductModel {
    id: string
    name: string
    quantity: number
    price: number
    status: string
    constructor(params: ProductModel = {} as ProductModel) {
        const {
            id = '',
            name = '',
            quantity = 0,
            price = 0,
            status = ''
        } = params;

        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.status = status;
    }
}
