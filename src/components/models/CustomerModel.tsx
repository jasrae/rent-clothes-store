export class CustomerModel {
    id: string = ""
    name: string = ""
    phone: string = ""
    address: string = ""

    constructor(params: CustomerModel = {} as CustomerModel) {
        const {
            id = '',
            name = '',
            phone = '',
            address = ''
        } = params;

        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }
}
