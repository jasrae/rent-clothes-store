import {combineReducers, createStore} from "redux";
import {OrderProductModel} from "./models/OrderProductModel";

interface ProductState {
    data: OrderProductModel;
}

interface ListProductState {
    data: OrderProductModel[];
}

const initialProductState: ProductState = {
    data: new OrderProductModel(),
}

const initialListProductState: ListProductState = {
    data: []
}

function productReducer(state: ProductState = initialProductState, action: any): ProductState {
    switch (action.type) {
        case 'update_product' :
            return { ...state, data: action.payload};
        default:
            return state;
    }
}

function listProductReducer(state: ListProductState = initialListProductState, action: any): ListProductState {
    switch (action.type) {
        case 'update_list_product' :
            return {...state, data: action.payload};
        default:
            return state;
    }
}

interface AppState {
    product: ProductState,
    listProduct: ListProductState
}

const rootReducer = combineReducers({
    product: productReducer,
    listProduct: listProductReducer,
});

const store = createStore(rootReducer);

export default store;

export type {AppState};
