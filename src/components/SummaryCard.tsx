import React from "react";

interface SummaryCardProps {
    "label" : string,
    "value" : any
}

function SummaryCard({label, value} : SummaryCardProps) {
    return (
        <div className="w-full py-6 rounded-lg shadow shadow-gray-800 ml-2 mr-2 bg-slate-300">
            <div>
                <label className="text-lg font-bold">{label}</label>
            </div>
            <div>
                <label className="text-3xl font-bold">{value.toString()}</label>
            </div>
        </div>
    );
}

export default SummaryCard;
