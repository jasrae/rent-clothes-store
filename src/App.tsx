import React from 'react';
import './App.css';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Expense from "./components/pages/Expense";
import Archive from "./components/pages/Archive";
import {LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import Product from "./components/pages/Product";
import Report from "./components/pages/Report";
import Home from "./components/pages/Home";
import Customer from './components/pages/Customer';
import Order from './components/pages/Order';

function App() {
  return (
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <div className="App">
          <BrowserRouter>
            <Routes>
              <Route path="/*" element={<Home/>}/>
              <Route path="/customer" element={<Customer/>}/>
              <Route path="/order" element={<Order/>}/>
              <Route path="/product" element={<Product/>}/>
              <Route path="/archive" element={<Archive/>}/>
              <Route path="/expense" element={<Expense/>}/>
              <Route path="/report" element={<Report/>}/>
            </Routes>
          </BrowserRouter>
        </div>
      </LocalizationProvider>

  );
}

export default App;
